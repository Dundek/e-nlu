"use strict"

var validator = require("validator");
var { prompt } = require('inquirer');
var path = require("path");
var chalk = require("chalk");
let lbtools = require("../lib/lb-tools");
let fstools = require("../lib/fs-tools");
let fs = require("fs");
const { spawn } = require('child_process');

let dockerController = require("./dockerController");
let rasaController = require("./rasaController");

let specialCharactersRegex = /^[_A-z0-9]*$/g;

module.exports = {
    /**
     * init
     */
    init: async() => {
        await fstools.init();
        await dockerController.init();
        await rasaController.init();
    },
    /**
     * newProject
     */
    newProject: async() => {
        let questions = [{
            type: 'input',
            name: 'project',
            message: 'Name of the new project:',
            validate: async(data) => {
                if (data.trim().length == 0) {
                    return "Required";
                } else if (data.match(specialCharactersRegex) == null) {
                    return "No special or spaces characters allowed";
                } else {
                    let existingProjects = await lbtools.find("Projects", {
                        "where": {
                            "name": data
                        }
                    });
                    if (existingProjects.length > 0) {
                        return "This project name already exists";
                    } else {
                        return true;
                    }
                }
            }
        }];

        let responses = await prompt(questions);

        await fstools.createProject(responses.project);

        await lbtools.upsert("Projects", {
            "name": responses.project
        });
    },
    /**
     * trainModel
     */
    trainModel: async() => {
        let existingProjects = await lbtools.find("Projects", {});

        if (existingProjects.length == 0) {
            console.log("There are no projects to train models for yet.");
            return;
        }

        let questions = [{
            type: 'list',
            name: 'project',
            message: 'Train model for project:',
            choices: []
        }];
        questions[0].choices = existingProjects.map(p => p.name);

        let responses = await prompt(questions);

        await rasaController.trainModel(responses.project);
    },
    /**
     * deleteProject
     */
    deleteProject: async() => {
        let existingProjects = await lbtools.find("Projects", {});

        if (existingProjects.length == 0) {
            console.log("There are no projects to delete.");
            return;
        }

        let questions = [{
            type: 'list',
            name: 'project',
            message: 'Delete project:',
            choices: []
        }];
        questions[0].choices = existingProjects.map(p => p.name);

        let responses = await prompt(questions);

        await fstools.deleteProject(responses.project);

        await lbtools.deleteWhere("Projects", {
            "name": responses.project
        });
    },
    /**
     * editTrainingSet
     */
    editTrainingSet: async() => {
        let existingProjects = await lbtools.find("Projects", {});

        if (existingProjects.length == 0) {
            console.log("There are no projects to train models for yet.");
            return;
        }

        let questions = [{
            type: 'list',
            name: 'project',
            message: 'Train model for project:',
            choices: []
        }];
        questions[0].choices = existingProjects.map(p => p.name);

        let responses = await prompt(questions);

        await fstools.init();

        var editorSpawn = require('child_process').spawn(
            "nano", [path.join(fstools.getProjectBasePath(), responses.project, responses.project + ".chatito")], {
                stdio: 'inherit',
                detached: true,
                name: 'xterm-color',
                cols: process.stdout.columns,
            });
    },
    /**
     * configureComposeFileDir
     */
    configureComposeFileDir: async() => {
        let composeDirSetting = await lbtools.findOne("Settings", {
            "where": {
                "key": "COMPOSE_DIR"
            }
        });

        let questions = [{
            type: 'input',
            name: 'composeDir',
            message: 'Path of the docker-compose dir:',
            validate: async(data) => {
                if (data.trim().length == 0) {
                    return "Required";
                } else {
                    try {
                        fs.statSync(data);
                        return true;
                    } catch (e) {
                        return "Path does not exist";
                    }
                }
            }
        }];

        if (composeDirSetting) {
            questions[0].default = composeDirSetting.value;
        }

        let responses = await prompt(questions);
        responses.composeDir = responses.composeDir.trim();
        await lbtools.upsert("Settings", {
            "key": "COMPOSE_DIR",
            "value": responses.composeDir
        });
    },
    /**
     * configureHost
     */
    configureHost: async() => {
        let previousSetting = await lbtools.findOne("Settings", {
            "where": {
                "key": "RASA_HOST"
            }
        });

        let questions = [{
            type: 'input',
            name: 'host',
            message: 'Rasa url:',
            default: "http://localhost:5000",
            validate: async(data) => {
                if (data.trim().length == 0) {
                    return "Required";
                } else {
                    return true;
                }
            }
        }];

        if (previousSetting) {
            questions[0].default = previousSetting.value;
        }

        let responses = await prompt(questions);
        responses.host = responses.host.trim();
        if (responses.host.lastIndexOf("/") == (responses.host.length - 1)) {
            responses.host = responses.host.substring(0, responses.host.length - 1);
        }

        await lbtools.upsert("Settings", {
            "key": "RASA_HOST",
            "value": responses.host
        });
    },
    /**
     * configureAuth
     */
    configureAuth: async() => {
        let previousSetting = await lbtools.findOne("Settings", {
            "where": {
                "key": "AUTH"
            }
        });

        let questions = [{
            type: 'input',
            name: 'auth',
            message: 'Basic auth if required (user:pass):',
            validate: async(data) => {
                if (data.trim().length > 0 && data.indexOf(":") == -1) {
                    return "Invalide, expected: <username>:<password>";
                } else {
                    return true;
                }
            }
        }];

        if (previousSetting) {
            questions[0].default = previousSetting.value;
        }

        let responses = await prompt(questions);
        responses.auth = responses.auth.trim();
        await lbtools.upsert("Settings", {
            "key": "AUTH",
            "value": responses.auth
        });
    }
}