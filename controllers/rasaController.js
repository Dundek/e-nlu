"use strict"

let dockerController = require("./dockerController");
var { prompt } = require('inquirer');
var path = require('path');
var fs = require('fs');
var chalk = require('chalk');
var request = require('request-promise');
var self = require('./rasaController');
const ora = require('ora');
const chatito = require("chatito");

const compose = require("docker-compose");

const fstools = require("../lib/fs-tools");
const lbtools = require("../lib/lb-tools");

const spinner = ora('');
spinner.color = 'yellow';

let basicAuth = {};
let rasaUrl = null;
let settings = null;

exports.init = async() => {
    spinner.start();
    spinner.text = 'Initializing e-nlu...';


    settings = await lbtools.find("Settings", {});
    let _auth = settings.find(s => s.key == "AUTH");
    rasaUrl = settings.find(s => s.key == "RASA_HOST").value;

    if (_auth.value.trim().length > 0) {
        _auth = _auth.value.trim().split(":");
        basicAuth = {
            'user': _auth[0],
            'pass': _auth[1],
            'sendImmediately': false
        };
    }

    try {
        // Get RASA status
        let status = await self.getContainerStatus();

        // RASA not installed
        if (!status.online) {
            await self.startCompose();
            status = await self.getContainerStatus(true);
        }
        // RASA Duckling container is stopped or paused
        if (status.ducklingContainer.state == "stopped" || status.ducklingContainer.state == "exited") {
            spinner.text = 'Starting RASA Duckling container...';
            await dockerController.startContainer(status.ducklingContainer);
        } else if (status.ducklingContainer.state == "paused") {
            spinner.text = 'Unpausing RASA Duckling container...';
            await dockerController.unpauseContainer(status.ducklingContainer);
        }

        // RASA container is stopped or paused
        if (status.rasaContainer.state == "stopped" || status.rasaContainer.state == "exited") {
            spinner.text = 'Starting RASA container...';
            await dockerController.startContainer(status.rasaContainer);
        } else if (status.rasaContainer.state == "paused") {
            spinner.text = 'Unpausing RASA container...';
            await dockerController.unpauseContainer(status.rasaContainer);
        }

        spinner.stop();
    } catch (e) {
        spinner.stop();
        throw e;
    }
}

/**
 * loadModelsToMemory
 */
exports.loadModelsToMemory = async() => {
    spinner.start();
    spinner.text = 'Loading models...';

    let response = await request({
        method: 'GET',
        uri: rasaUrl + '/status',
        auth: basicAuth,
        json: true // Automatically stringifies the body to JSON
    });

    for (let modelName in response.available_projects) {
        if (modelName != "default") {
            if (response.available_projects[modelName].status == "ready") {
                let loaded = response.available_projects[modelName].loaded_models.find(m => m != "fallback") ? true : false;
                if (!loaded) {
                    spinner.text = 'Loading model ' + modelName + '...';
                    let loadResponse = await request({
                        method: 'GET',
                        uri: rasaUrl + '/parse?q=hello&project=' + modelName,
                        auth: basicAuth,
                        timeout: 120 * 1000,
                        json: true // Automatically stringifies the body to JSON
                    });
                }
            }
        }
    }
    spinner.stop();
}

/**
 * trainModel
 * @param {*} delay 
 */
exports.trainModel = (projectName) => {
    return new Promise((resolve, reject) => {
        spinner.text = 'Building training set...';
        spinner.start();
        (async() => {
            let start = null;
            let timeout = 120 * 1000;
            try {
                let dslDefinitionString = fs.readFileSync(path.join(fstools.getProjectBasePath(), projectName, projectName + ".chatito")).toString().trim();
                const dataset = chatito.datasetFromString(dslDefinitionString);
                start = new Date().getTime();

                let rasaConfigTensorflow = fs.readFileSync(path.join(__basedir, "resources", "training", "config_tensorflow.yml")).toString();
                rasaConfigTensorflow = rasaConfigTensorflow.replace("_data_", JSON.stringify(dataset, null, 4));

                let rasaConfigSpacy = fs.readFileSync(path.join(__basedir, "resources", "training", "config_spacy.yml")).toString();
                rasaConfigSpacy = rasaConfigSpacy.replace("_data_", JSON.stringify(dataset, null, 4));
                rasaConfigSpacy = rasaConfigSpacy.replace("_ducklingip_", "enlu_rasa_duckling");

                spinner.text = 'Training intents ' + projectName + ' model, this might take some minutes...';
                let response = await request({
                    method: 'POST',
                    uri: `${rasaUrl}/train?project=${projectName}_tensorflow`,
                    auth: basicAuth,
                    body: rasaConfigTensorflow,
                    headers: {
                        'content-type': 'application/x-yml'
                    },
                    timeout: timeout
                });
                spinner.text = 'Training entities dockersuggar model, this might take some minutes...';
                response = await request({
                    method: 'POST',
                    uri: `${rasaUrl}/train?project=${projectName}_spacy`,
                    auth: basicAuth,
                    body: rasaConfigSpacy,
                    headers: {
                        'content-type': 'application/x-yml'
                    },
                    timeout: timeout
                });

                await self.loadModelsToMemory();

                resolve(true);
            } catch (e) {
                spinner.stop();
                // Detect timeout error, does not cause model from being trained
                if (start && (new Date().getTime() - start) >= timeout) {
                    resolve(false);
                } else {
                    reject(e);
                }
            }
        })();
    });
}

/**
 * getModelStatus
 */
exports.getContainerStatus = (delay) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            (async() => {
                let containers = null;
                let rasaContainer = null;
                let ducklingContainer = null;

                try {
                    containers = await dockerController.listContainers();

                    rasaContainer = containers.find(r => r.names.indexOf("_enlu_rasa_1") != -1);
                    ducklingContainer = containers.find(r => r.names.indexOf("_enlu_rasa_duckling_1") != -1);

                    if (!rasaContainer || !ducklingContainer) {
                        resolve({
                            "online": false,
                            "rasaContainer": null,
                            "ducklingContainer": ducklingContainer
                        });
                        return;
                    }
                    resolve({
                        "online": true,
                        "rasaContainer": rasaContainer,
                        "ducklingContainer": ducklingContainer
                    });
                } catch (e) {
                    resolve({
                        "online": false,
                        "rasaContainer": rasaContainer,
                        "ducklingContainer": ducklingContainer
                    });
                }
            })();
        }, delay ? delay : 0);
    });
}

/**
 * startCompose
 */
exports.startCompose = async() => {
    spinner.text = 'Docker-compose up...';
    spinner.start();

    let dockerComposePath = await lbtools.findOne("Settings", {
        "where": {
            "key": "COMPOSE_DIR"
        }
    });
    if (!dockerComposePath) {
        spinner.stop();
        throw new Error("Docker-compose manifest directory not set");
    }
    return compose.up({ cwd: dockerComposePath.value, log: true });
}