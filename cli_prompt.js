"use strict"

const promptController = require("./controllers/promptController");
const rasaController = require("./controllers/rasaController");
const program = require('./commander-custom/commander');
var chalk = require("chalk");
var request = require("request-promise");
var figlet = require('figlet');
const editor = require('tiny-cli-editor');
var lbtools = require('./lib/lb-tools');

module.exports = {
    start: async() => {
        program
            .version('1.0.0')
            .description("\n" + figlet.textSync('e-nlu'));

        /**
         * INIT
         * @param {*} p 
         */
        let init = async(p) => {
            let setting = await lbtools.find("Settings", {});

            if (!setting.find(s => s.key == "COMPOSE_DIR")) {
                await promptController.configureComposeFileDir();
            }
            if (!setting.find(s => s.key == "RASA_HOST")) {
                await promptController.configureHost();
            }
            if (!setting.find(s => s.key == "AUTH")) {
                await promptController.configureAuth();
            }
            await promptController.init();
        }

        let cmdValue = null;

        /**
         * COMMANDS: REMOTE STUFF 
         */
        program
            .command('newProject')
            .description('Create a new project')
            .action(() => {
                cmdValue = "newProject";
                (async() => {
                    try {
                        await init(program);
                        console.log("");
                        await promptController.newProject();
                        console.log("Project created!");
                        process.exit(0);
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                })();
            });

        program
            .command('deleteProject')
            .description('Delete a project')
            .action(() => {
                cmdValue = "deleteProject";
                (async() => {
                    try {
                        await init(program);
                        console.log("");
                        await promptController.deleteProject();
                        console.log("Project deleted!");
                        process.exit(0);
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                })();
            });

        program
            .command('editTrainingSet')
            .description('Edit a project training set')
            .action(() => {
                cmdValue = "editTrainingSet";
                (async() => {
                    try {
                        await promptController.editTrainingSet();
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                })();
            });

        program
            .command('trainModel')
            .description('Train a model')
            .action(() => {
                cmdValue = "trainModel";
                (async() => {
                    try {
                        await init(program);
                        console.log("");
                        await promptController.trainModel();
                        setTimeout(() => {
                            console.log("Done!");
                            process.exit(0);
                        }, 200);
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                })();
            });

        program
            .command('loadModels')
            .description('Load models to memory')
            .action(() => {
                cmdValue = "loadModels";
                (async() => {
                    try {
                        await init(program);
                        console.log("Loading current models...");
                        let response = await request({
                            method: 'GET',
                            uri: 'http://localhost:5000/status',
                            json: true // Automatically stringifies the body to JSON
                        });

                        for (let modelName in response.available_projects) {
                            if (modelName != "default") {
                                if (response.available_projects[modelName].status == "ready") {
                                    let loaded = response.available_projects[modelName].loaded_models.find(m => m != "fallback") ? true : false;
                                    // if (!loaded) {
                                    let loadResponse = await request({
                                        method: 'GET',
                                        uri: 'http://localhost:5000/parse?q=hello&project=' + modelName,
                                        timeout: 120 * 1000,
                                        json: true // Automatically stringifies the body to JSON
                                    });
                                }
                            }
                        }
                        setTimeout(() => {
                            console.log("Done!");
                            process.exit(0);
                        }, 200);
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                })();
            });

        program
            .command('settings')
            .description('Edit global settings')
            .action(() => {
                cmdValue = "settings";
                (async() => {
                    try {
                        await promptController.configureComposeFileDir();
                        await promptController.configureHost();
                        await promptController.configureAuth();
                    } catch (e) {
                        console.log("ERROR: ", e);
                    }
                })();
            });

        program.parse(process.argv);

        // If no command identified
        if (cmdValue === null) {
            (async() => {
                try {
                    await init(program);
                    rasaController.loadModelsToMemory();
                    setTimeout(() => {
                        console.log("e-nlu is ready for business!");
                    }, 200);
                } catch (e) {
                    console.log("ERROR: ", e);
                }
            })();
        }
    }
}