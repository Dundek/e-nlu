"use strict";

module.exports = {
    /**
     * find
     */
    find: async(modelName, search) => {
        return new Promise((resolve, reject) => {
            require("../server/server").models[modelName].find(search, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    },
    /**
     * findOne
     */
    findOne: async(modelName, search) => {
        return new Promise((resolve, reject) => {
            require("../server/server").models[modelName].findOne(search, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    },
    /**
     * upsert
     */
    upsert: async(modelName, object) => {
        return new Promise((resolve, reject) => {
            require("../server/server").models[modelName].upsert(object, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    },
    /**
     * delete
     */
    deleteWhere: async(modelName, object) => {
        return new Promise((resolve, reject) => {
            require("../server/server").models[modelName].destroyAll(object, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }
}