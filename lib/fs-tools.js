"use strict";

const mkdirp = require('mkdirp');
const path = require("path");
const fs = require("fs");
const rimraf = require('rimraf');
const os = require('os');
const lbtools = require("./lb-tools");

const rasaModelsPath = path.join(os.homedir(), "e-nlu", "rasaModels");
const projectsPath = path.join(os.homedir(), "e-nlu", "projects");

module.exports = {
    /**
     * init
     */
    init: async() => {
        return new Promise((resolve, reject) => {
            mkdirp(rasaModelsPath, function(err) {
                if (err) return reject(err);
                mkdirp(projectsPath, function(err) {
                    if (err) return reject(err);
                    resolve();
                });
            });
        });
    },
    /**
     * 
     */
    getRasaModelsPath: () => {
        return rasaModelsPath;
    },
    /**
     * 
     */
    getProjectBasePath: () => {
        return projectsPath;
    },
    /**
     * createProject
     */
    createProject: async(projectName) => {
        try {
            let projectDir = path.join(projectsPath, projectName);
            fs.mkdirSync(projectDir);
            fs.createReadStream(path.join(__basedir, "resources", "training", "template.chatito")).pipe(fs.createWriteStream(path.join(projectDir, projectName + '.chatito')));
        } catch (err) {
            throw err
        }
    },
    /**
     * deleteProject
     */
    deleteProject: async(projectName) => {
        try {
            rimraf.sync(path.join(projectsPath, projectName));
            rimraf.sync(path.join(projectsPath, projectName + "_tensorflow"));
            rimraf.sync(path.join(projectsPath, projectName + "_spacy"));
        } catch (err) {
            throw err
        }
    }
}