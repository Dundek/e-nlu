# Easy NLU

This package is based on RASA NLU, with an extra layer of administration to simplify the creation of projects, training sets and usage.

## Prereq

You need to install docker-compose for this to work.  
If you want to use e-nlu to edit your training sets, then make sure you installed the terminal emulator ```nano```.

## Nginx - reverse proxy

If you use this behind a Nginx reverse proxy, here is an example config:

```
upstream rasanlu  {
    server enlu_rasa:5000;
}

...

server {
    ...

    # RASA_NLU
    location /rasanlu/ {
        proxy_pass http://rasanlu/;
    }

    ...
}
```

## Docker-compose file

Create your ```docker-compose.yml``` file somewhere on your machine, copy/paste the following content to it:

```
version: "3"
services:
  # -------------- RASA_NLU
  enlu_rasa:
    image: rasa/rasa_nlu
    restart: always
    volumes:
      - <path to home>/e-nlu/projects:/app/projects

  # -------------- RASA_NLU_DUCKLING
  enlu_rasa_duckling:
    image: rasa/duckling
    restart: always
```

> Replace the ```<path to home>``` part with your absolute home directory path. This is where e-nlu stores all it's files.  

On first usage, e-nlu will ask you for the directory where this file is stored.

## Usage

### Commands used to manage projects

| Command               | Description |
| --------------------- | ------------|
| newProject      | Create a new NLU project. This will create a default training set that you can then edit as wished. |
| editTrainingSet | This will open the training set file for a specific project using the 'nano' editor in your terminal. |
| trainModel      | This will train a specific model for a specific project, based on it's training set. |
| loadModels      | This is optional, and can be used to load all models into memory. This will avoid a long delay the first time a model is used. |
| settings        | Update global settings |

> 

### Call NLU model

To call a project model, use the following REST API (GET):

```
http(s)://<rasa_url>/parse?q=<utterance>&project=<project_name>
```
